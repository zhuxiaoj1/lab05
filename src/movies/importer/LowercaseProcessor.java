// Danilo Zhu 1943382 Lab 5
package movies.importer;

import java.util.*;

public class LowercaseProcessor extends Processor {
	public LowercaseProcessor(String source, String output) {
		super(source, output, true);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();

		for (String i : input) {
			asLower.add(i.toLowerCase());
		}

		return asLower;
	}
}