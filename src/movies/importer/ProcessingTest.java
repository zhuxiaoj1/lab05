// Danilo Zhu 1943382 Lab 5
package movies.importer;

import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException {
		// Done on Linux, I apologize for the weird paths.

		LowercaseProcessor toLower = new LowercaseProcessor("/home/xiaoju/Documents/java310/txtfiles/",
				"/home/xiaoju/Documents/java310/intermediate/");
		toLower.execute();

		RemoveDuplicates removeDupe = new RemoveDuplicates("/home/xiaoju/Documents/java310/intermediate/",
				"/home/xiaoju/Documents/java310/outputfiles/");
		removeDupe.execute();
	}
}