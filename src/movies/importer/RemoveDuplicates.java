// Danilo Zhu 1943382 Lab 5
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	public RemoveDuplicates(String source, String output) {
		super(source, output, false);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> copy = new ArrayList<String>();

		for (String i : input) {
			if (!copy.contains(i)) {
				copy.add(i);
			}
		}

		return copy;
	}
}